let timeBegan = null;
let timeStop = null;
let startInterval = null;
let flag = false;

document.querySelector (".language-button").style.direction = "ltr";
document.querySelector (".typing-box").style.direction = "ltr"

let randomText = ["silence is the most powerful scream","never give up","never judge people","live for yourself"];
let orginalText = document.querySelector (".orginalText");
orginalText.innerHTML = randomText [Math.floor (Math.random () * randomText.length)];
document.querySelector(".english-btn").addEventListener ("click", generate()) 

function generate () {
    document.querySelector (".typing-box").addEventListener ("keypress", () => {
        if (!flag) {
            startTimer()
            flag = true;
        }
    });
    
    startTimer = () => {
        if ( timeBegan === null) {
            timeBegan = new Date ()
        }
        if (timeStop !== null) {
            timeStop = null;
        }
        startInterval = setInterval (showTimer, 10)
    };

    
    function showTimer () {
        let date = new Date ();
        let timeElapsed = new Date (date - timeBegan);

        let minutes = timeElapsed.getUTCMinutes ();
        let seconds = timeElapsed.getUTCSeconds ();
        let millseconds = timeElapsed.getUTCMilliseconds ();
        millseconds = Math.floor (millseconds /10)

        minutes = (minutes < 10) ? `0${minutes}` : minutes;
        seconds = (seconds < 10) ? `0${seconds}` : seconds;
        millseconds = (millseconds < 10) ? `0${millseconds}` : millseconds;

        let time = `${minutes}:${seconds}:${millseconds}`;

        let timer = document.querySelector (".timer").innerHTML = time;
    }

    document.querySelector ("#typing-input").addEventListener ("keyup", () => {
        stopTimer ();
    });
    
    stopTimer = () => {
        let text = document.querySelector ("#typing-input");
        let orginalText = document.querySelector (".orginalText").innerHTML

        if (text.value == orginalText) {
            clearInterval (startInterval);
            timeStop = new Date()
            document.querySelector (".reset-btn").style.background = "#CD295A";
            document.querySelector (".reset-btn").style.color = "#fff";
        }
    }

    document.querySelector (".reset-btn").addEventListener ("click", () => {
        timeBegan = null;
        timeStop = null;
        startInterval = null;
        flag = false;
        clearInterval (startInterval);
        document.querySelector ("#typing-input").InnerText = "";
    })
}